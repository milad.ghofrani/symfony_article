<?php

namespace App\Factory;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Carbon\Carbon;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Article|Proxy findOrCreate(array $attributes)
 * @method static Article|Proxy random()
 * @method static Article[]|Proxy[] randomSet(int $number)
 * @method static Article[]|Proxy[] randomRange(int $min, int $max)
 * @method static ArticleRepository|RepositoryProxy repository()
 * @method Article|Proxy create($attributes = [])
 * @method Article[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class ArticleFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
//        $publishedAt = (rand(1,10) % 2 == 0) ? self::faker()->dateTimeBetween('-30 days', '-1 days') : null ;
        $publishedAt = self::faker()->dateTimeBetween('-30 days', '-1 days');
        $authors = ["Milad Ghofrani","Amin Khoshzahmat","Heeman Abdollahi Haghi"];
        $categories = ["Sport","Politic","Travel"];

        return [
            'title' => self::faker()->sentence(6),
            'content' => self::faker()->paragraphs(4, true),
            'publishedAt' => $publishedAt,
            'category' => $categories[array_rand($categories,1)],
            'author' => $authors[array_rand($authors,1)],
            'imageFilename' => rand(1,12).".jpg",
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(Article $article) {})
        ;
    }

    protected static function getClass(): string
    {
        return Article::class;
    }
}
