<?php


namespace App\Service;


use App\Helper\LoggerTrait;
use Michelf\MarkdownInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MarkdownHelper
{
    use LoggerTrait;

    private $cache;
    private $markdown;
    private $logger;
    private $isDebug;

    public function __construct(CacheInterface $cache, MarkdownInterface $markdown, bool $isDebug)
    {
        $this->cache = $cache;
        $this->markdown = $markdown;
        $this->isDebug = $isDebug;
    }

    public function parse($string){

        $this->logInfo("Markdown log is ok");

        if($this->isDebug){
            return $this->markdown->transform($string);
        }

        $item = $this->cache->getItem("markdown_".md5($string));
        if(!$item->isHit()){
            $item->set($this->markdown->transform($string));
            $this->cache->save($item);
        }
        return $item->get();
    }
}