<?php


namespace App\Controller;


use App\Entity\Article;
use App\Factory\ArticleFactory;
use App\Factory\CommentFactory;
use App\Factory\TagFactory;
use App\Factory\UserFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleAdminController extends AbstractController
{
    /**
     * @Route("/admin/article/store")
     */
    public function store(){

//        ArticleFactory::new()->createMany(10);

        /*CommentFactory::new()->many(50)
        ->create(function() {
            return ['article' => ArticleFactory::random()];
        });*/

//        TagFactory::new()->createMany(10);

        /*ArticleFactory::new()->many(5)
            ->create(function() {
                return ['tags' => TagFactory::randomSet(3)];
            });*/

        UserFactory::new()->createMany(15);

        return new Response(sprintf("The new article has been created"));
    }
}