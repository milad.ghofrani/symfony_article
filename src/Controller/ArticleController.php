<?php


namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/",name="article.index")
     */
    public function index(ArticleRepository $repository){

        $articles = $repository->findByLatestPublished();

        return $this->render("article/index.html.twig",[
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/article/{slug}", name="article.show")
     */
    public function show(Article $article, EntityManagerInterface $em){

        $article->incrementViews();
        $em->flush();

        return $this->render("article/show.html.twig",[
            'article' => $article
        ]);
    }
}